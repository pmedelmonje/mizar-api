from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import Response, JSONResponse
from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
import uvicorn
from vehicles import vehicles_router
from vehicles.models import *
from settings import settings
from settings.db import Base, engine, SessionLocal

app = FastAPI(
    title = "Mizar API - Registro de Vehículos",
    description = "Registro interno de vehículos del Centro Misionero Los Dos Olivos",
    version = "0.1.3",
    #terms_of_service = "https://www.example.com",
    contact = {
        "name": "Pedro Medel",
        "url": "https://www.example.com",
        "email": "medelmonje0913@vivaldi.net",
        "phone": +56999253800
    }
)

### Uncomment only if not using Alembic
Base.metadata.create_all(bind = engine)

### Middleware

origins = settings.hosts

app.add_middleware(
    CORSMiddleware,
    allow_origins = origins,
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"]
)


@app.middleware("http")
async def http_error_handler(request: Request, call_next) -> Response | JSONResponse:
    
    try:
        return await call_next(request)
    except Exception as e:
        return JSONResponse(
            status_code = 500,
            content = {
                "exc": f"{str(e)}"
            } 
        )
    

### Routers

app.include_router(vehicles_router.router)


if __name__ == "__main__":
    uvicorn.run("main:app", port = 8000, reload = True)