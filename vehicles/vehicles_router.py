from typing import Union
from typing_extensions import Annotated
from fastapi import APIRouter, Depends, Request, Header, status
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from .schemas import *
from .crud import *
from settings.db import get_db
from settings.settings import master_token
from utils.validators import validate_token, validate_registration


router = APIRouter(
    prefix= "/vehicles",
    tags = ["vehicles"]
)


@router.get("/test", include_in_schema = False)
def test(token: str = Depends(validate_token)):
    return token


@router.get("/", status_code = 200, response_model = List[VehicleResponse])
def show_vehicles(
    skip: int = 0, 
    limit: int = 100,
    db: Session = Depends(get_db),
    token: str = Depends(validate_token),
):
    
    vehicles = list_vehicles(db, skip, limit)
    return vehicles


@router.get("/{id}", status_code = 200, response_model = VehicleResponse)
def get_vehicle_info(
    id: int, 
    db: Session = Depends(get_db),
    token: str = Depends(validate_token)
):
    existing_vehicle = get_vehicle_by_id(db, id)
    if not existing_vehicle:
        raise HTTPException(
            status_code = 404,
            detail = "Vehículo no encontrado"
        )
        
    return existing_vehicle


@router.post("/", status_code = 201, response_model = VehicleResponse)
def register_vehicle(
    vehicle: VehicleCreate, 
    db: Session = Depends(get_db),
    token: str = Depends(validate_token)
):
    existing_vehicle = get_vehicle_by_registration(db, vehicle.registration)
    
    if existing_vehicle:
        raise HTTPException(
            status_code = 400,
            detail = "Ya existe un Vehículo con esa patente"
        )
        
    if not validate_registration(vehicle.registration):
        raise HTTPException(
            status_code = 400,
            detail = "Formato de patente no válido"
        )
        
    try:
        new_vehicle = create_vehicle(db, vehicle.dict())
        return new_vehicle
    
    except Exception as e:
        raise HTTPException(
            status_code = 400,
            detail = str(e)
        )
    
    
@router.put("/{id}", status_code = 200, response_model = VehicleResponse)
def update_vehicle(
    id: int, 
    vehicle: VehicleUpdate, 
    db: Session = Depends(get_db),
    token: str = Depends(validate_token)
):
    existing_vehicle = get_vehicle_by_registration(db, vehicle.registration)
    if existing_vehicle and existing_vehicle.id != id:
        raise HTTPException(
            status_code = 400,
            detail = "La Patente ya está siendo utilizada en otro registro"
        )
        
    updated_vehicle = update_vehicle_by_id(db, id, vehicle.dict())
    if not updated_vehicle:
        raise HTTPException(
            status_code = 404,
            detail = "Vehículo no encontrado"
        )
        
    return updated_vehicle


@router.delete("/{id}", response_model = ResponseSchema)
def remove_vehicle(
    id: UUID,
    db: Session = Depends(get_db),
    token: str = Depends(validate_token)
):
    existing_vehicle = get_vehicle_by_id(db, id)
    if not existing_vehicle:
        raise HTTPException(
            status_code = 404,
            detail = "Vehículo no encontrado"
        )
    
    vehicle_registration = existing_vehicle.registration
        
    delete_vehicle_by_id(db, existing_vehicle.id)
    return JSONResponse(
        status_code = status.HTTP_204_NO_CONTENT,
        content = {"detail": f"Vehículo {vehicle_registration} eliminado correctamente."}
    )
        
    
    
    