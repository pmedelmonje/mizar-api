from uuid import UUID
from sqlalchemy.orm import Session
from slugify import slugify
from .models import Vehicle
from .schemas import *


def list_vehicles(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Vehicle).offset(skip).limit(limit).all()


def get_vehicles_by_brand(db: Session, brand: str):
    return db.query(Vehicle).filter(Vehicle.brand == brand).all()


def get_vehicle_by_id(db: Session, id: int):
    return db.query(Vehicle).filter(Vehicle.id == id).first()


def get_vehicle_by_registration(db: Session, registration: str):
    return db.query(Vehicle).filter(Vehicle.registration == registration).first()


def get_vehicle_by_slug(db: Session, slug: str):
    return db.query(Vehicle).filter(Vehicle.slug == slug).first()


def create_vehicle(db: Session, vehicle_data: dict):
    new_vehicle = Vehicle(
        registration = vehicle_data["registration"],
        slug = slugify(vehicle_data["registration"]),
        brand = vehicle_data["brand"],
        model = vehicle_data["model"],
        color = vehicle_data["color"],
        owner = vehicle_data["owner"],
        phone = vehicle_data["phone"]
    )
    db.add(new_vehicle)
    db.commit()
    db.refresh(new_vehicle)
    return new_vehicle


def update_vehicle_by_id(db: Session, id: UUID, vehicle_data: dict):
    existing_vehicle = get_vehicle_by_id(db, id)
    if existing_vehicle:
        existing_vehicle.registration = vehicle_data["registration"]
        existing_vehicle.slug = slugify(vehicle_data["registration"])
        existing_vehicle.brand = vehicle_data["brand"]
        existing_vehicle.model = vehicle_data["model"]
        existing_vehicle.color = vehicle_data["color"]
        existing_vehicle.owner = vehicle_data["owner"]
        existing_vehicle.phone = vehicle_data["phone"]
        db.commit()
        db.refresh(existing_vehicle)
        return existing_vehicle
    return None


def delete_vehicle_by_id(db: Session, id: UUID):
    existing_vehicle = get_vehicle_by_id(db, id)
    if existing_vehicle:
        db.delete(existing_vehicle)
        db.commit()