import os
from dotenv import load_dotenv

### General

hosts = [
    "http://localhost", 
    "https://localhost", 
    "http://localhost:8000", 
    "http://localhost:8080",
    "file:///home/pmedel/Python/FastAPI/deneb_algedi/consumo_api/index.html"
]

### Variables de entorno

load_dotenv()

secret_key = os.getenv('SECRET_KEY')

master_token = os.getenv('MASTER_TOKEN')

db_user = os.getenv('DB_USER')
db_passwd = os.getenv('DB_PASSWORD')
db_server = os.getenv('DB_SERVER')
db_port = os.getenv('DB_PORT')
db_name = os.getenv('DB_NAME')
database_url = f"postgresql://{db_user}:{db_passwd}@{db_server}:{db_port}/{db_name}"